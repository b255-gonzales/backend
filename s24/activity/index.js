
let getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);


// Exponent Operator


// Template Literals

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

const [houseNumber, street, state, zipCode] = address;

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

console.log(`${animal.name} was a ${animal.species}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}.`);





// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

// Javascript Classes
numbers.forEach((numbers) =>{
	console.log(numbers);
});

const initval = 0;
const totalSumOfValues = numbers.reduce((accumulator, currentValue) => accumulator + currentValue, initval);

console.log(totalSumOfValues);


class Dog{
	constructor(name, age, breed){

	}
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}
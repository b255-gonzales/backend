//Number 3

db.singleroom.insertOne({
		"name": "single",
		"accommodates": 2,
		"price": 1000,
		"description": "A simple room with all the basic necessities",
		"room_available": 10,
		"isAvailable": false
 	});

//Number 4

db.multiplerooms.insertMany([
	{	
		"name": "double",
		"accommodates": 3,
		"price": 2000,
		"description": "A room fit for a small family on a vacation",
		"room_available": 5,
		"isAvailable": false

	},
	{
		"name": "queen",
		"accommodates": 4,
		"price": 4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"room_available": 15,
		"isAvailable": false
	}


	]);

// Number 5

db.multiplerooms.find({
	"name": "double"
});

// Number 6

db.multiplerooms.updateOne(
	{
		"name": "queen"
	},
	{
		$set:{
			"room_available": 0
		}
	}
);

// Number 7

db.multiplerooms.deleteMany({
	"room_available": 0
});
db.singleroom.deleteMany({
	"room_available": 0
});
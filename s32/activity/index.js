
let http = require("http");

http.createServer(function(request, response){

//1
	if(request.url == '/' && request.method == 'GET'){

		response.writeHead(200, {'Content-Type': 'text/plain'});


		
		response.end('Welcome to booking system');
	}
//2
	if(request.url == '/profile' && request.method == 'GET'){

		response.writeHead(200, {'Content-Type': 'text/plain'});


		
		response.end('Welcome to your profile');
	}
//3
	if(request.url == '/courses' && request.method == 'GET'){

		response.writeHead(200, {'Content-Type': 'text/plain'});


		
		response.end("Here's our courses available");
	}

//4
	if(request.url == '/addcourse' && request.method == 'POST'){

		response.writeHead(200, {'Content-Type': 'text/plain'});


		
		response.end('Add course to our resources');
	}

//5
	if(request.url == '/updatecourse' && request.method == 'PUT'){

		response.writeHead(200, {'Content-Type': 'text/plain'});


		
		response.end('Update a course to our resources');
	}

//6
	if(request.url == '/archivecourses' && request.method == 'DELETE'){

		response.writeHead(200, {'Content-Type': 'text/plain'});


		
		response.end('Archive courses to our resources');
	}



}).listen(4000);

console.log('Server is running at localhost:4000');
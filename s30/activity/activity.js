// Number 2

db.fruits.aggregate([
	{ $match : { onSale: true}},
	{ $count: "fruitOnSale"}
]);

// Number 3

db.fruits.aggregate([
  { $match: { onSale: true, stock: { $gte: 20 } } },
  { $count: "fruitOnSale"}
]);



// Number 4

db.fruits.aggregate([
  { $match : { onSale: true}},
  { $group: { _id: "$supplier_id", avg_price: { $avg: "$price"}}}

]);

// Number 5

db.fruits.aggregate([
  { $match : { onSale: true}},
  { $group: { _id: "$supplier_id", max_price: { $max: "$price"}}},

  	{ $sort: { _id: -1}}
]);

// Number 6

db.fruits.aggregate([
  { $match : { onSale: true}},
  { $group: { _id: "$supplier_id", min_price: { $min: "$price"}}},

  	{ $sort: { _id: -1}}
]);
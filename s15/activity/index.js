console.log("Hello World!");

let firstName = 'Donald';
let lastName = 'Trump';
let age = 30;
let hobbies = ['Running', 'Walking', 'Eating'];

let workAddress = {
	houseNumber: '101',
	street: 'Malayong Nayon',
	city: 'CityMall',
	state: 'San Fernando'
}

console.log('First Name: ' + firstName);
console.log('Last Name: ' + lastName);
console.log('Age: ' + age);
console.log('Hobbies:');
console.log(hobbies);
console.log('Work Address:');
console.log(workAddress);

//OBJECTIVE 2

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestfullName = "Bucky Barnes";
	console.log("My bestfriend is: " + bestfullName);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);

	try{
		module.exports = {
			firstName, lastName, age, hobbies, workAddress,
			fullName, currentAge, friends, profile, fullName2, lastLocation
		}
	} catch(err){
	}
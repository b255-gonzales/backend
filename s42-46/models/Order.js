const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId : {
		type: String,
		required: [true, "userId is required"]
	},

	totalAmount : {
		type: Number,
		default: 0,
	},

	purchasedOn : {
		type: Date,
		default: new Date()
	},


	products : [
					{
					productId: {
						type: String,
						required: [true, "Product ID is required"]
				
					},

					quantity: {
						type: Number,

					},

					productName: {
						type: String,
						required: [true, "Product Name is required"]
					},

					}]

});

module.exports = mongoose.model("Order", orderSchema);
const express = require("express");

const router = express.Router();

const userController = require("../controllers/user");
const productController = require("../controllers/product");

const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const auth = require("../auth");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

//Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.get("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);


	//Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController))
});




//USERCHECKOUT
router.put("/checkout", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: userData.id, 
		productId: req.body.productId

	}

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
})



router.put("/:userId/toAdmin", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){

	userController.changetoAdmin(req.params).then(resultFromController => res.send(resultFromController));

	} else{
		res.send(false);
	}


	
});




router.put("/checkout2", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: userData.id, 
		productId: req.body.productId,
		totalAmount: req.body.totalAmount,
		quantity : req.body.quantity
/*		products: [{

			productId : req.body.productId,

			quantity : req.body.quantity,
		}]
		*/

	}

	userController.checkout2(data).then(resultFromController => res.send(resultFromController));
})



router.get("/isUserAdmin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: userData.id
	}
	// // if(userData.isAdmin == true) {
	// // 	console.log("ABCDEFGH")
	// // }
	// // else{
	// //	}
	userController.isUserAdmin(data).then(resultFromController => res.send(resultFromController));



});




// Allows us to export the "router" object that will be accessed in our "index.js" file.
module.exports = router;


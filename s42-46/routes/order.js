const express = require("express");

const router = express.Router();

const userController = require("../controllers/user");
const productController = require("../controllers/product");
const orderController = require("../controllers/order");

const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Product");

const auth = require("../auth");




router.get("/allOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){

	orderController.getAuthenticatedOrders().then(resultFromController => res.send(resultFromController))

	} else{
		res.send(false);
	}


	
});

router.get("/pastcheckouts", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);


	console.log("userData.id");
	console.log(userData.id);
	console.log("userData.id");
	//Provides the user's ID for the getProfile controller method
	orderController.getUserCheckouts(userData.id).then(resultFromController => res.send(resultFromController))
});


router.put("/:orderId", auth.verify, (req, res) =>{

	const userData = auth.decode(req.headers.authorization);
	let data = {
		orderId : req.params.orderId,
		userId: userData.id, 
		productId: req.body.productId,

		quantity : req.body.quantity
/*		products: [{

			productId : req.body.productId,

			quantity : req.body.quantity,
		}]
		*/

	}

	orderController.addProduct(data).then(resultFromController => res.send(resultFromController))
})


/*router.put("/:orderId/changequantity", auth.verify, (req, res) =>{

	const userData = auth.decode(req.headers.authorization);
	let data = {
		orderId : req.params.orderId,
		userId: userData.id, 
		productId: req.body.productId,

		quantity : req.body.quantity


	}

	orderController.changeQuantity(data).then(resultFromController => res.send(resultFromController))
})
*/


module.exports = router;

const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");
const Order = require("../models/Order");

// Route for creating a product without admin
router.post("/", (req, res) => {
	productController.addProduct(req.body).then(resultFromController => res.send (resultFromController));
})



router.post("/admin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData.isAdmin + "isAdmin")
	if(userData.isAdmin == true){
		console.log(req.body)
		productController.addProduct(req.body).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send(false);
	}
});

// Route for retrieving all the courses

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});


// Route for retrieving all Active courses
// Middleware for verifying JWT is not required because users who are not logged in should also be able to view the courses
router.get("/active", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending upon the information provided

router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all parameters provided via the url
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})



router.put("/:productId", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else{
		res.send(false);
	}


	
})

//S40 ACTIVITY
// Route to archiving a course
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:productId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else{
		res.send(false);
	}


	
});


router.put("/:productId/activate", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		
		productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else{
		res.send(false);
	}

	
	
});




//TRY TRY

router.get(":productId/trial", auth.verify, (req, res) =>{
	productController.retrieveProductusingID(req.params.productId).then(resultFromController => res.send(resultFromController))
})

module.exports = router;
const Product = require("../models/Product");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database

*/

module.exports.addProduct = (reqBody) => {
	// Create a variable "newCourse" and instantiates a new "Course" object using the mongoose model

	let newProduct = new Product ({

		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	// Saves the created object to our database
	return newProduct.save().then((products, error) => {

		// Course creation not successful/failed
		if(error) {
			return false;

		// Course creation successful
		} else {
			return true;
		}

	})
}

// course route na getAll

/*
	Steps:
	1) Retrieve all the courses from the database
*/

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// Retrieve all ACTIVE courses
/*
	Steps:
	1) Retrieve all the courses from the database with the property of "isActive" to true

*/

module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

// Retrieving a specific course
/*
	Steps:
	1) Retrieve the course that matches the course ID provided from the URL

*/

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
}

module.exports.getProductuser = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	});
}


// Update a course
/*

	Steps:
	1) Create a variable "updateCourse" which will contain the information retrieved from the request body
	2) Find and update the course using the course ID retrieved from the params property and the variable "updateCourse" containing the information from the request body
*/

// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateProduct = (reqParams, reqBody) => {
	//Specify the files/properties of the document to be updated

	let updateProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}


// ACTIVITY S40

// Archive a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return false;

		} else {

			return true;

		}

	});
};


module.exports.activateProduct = (reqParams) => {

	let updateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return false;

		} else {

			return true;

		}

	});
};
// Try 

module.exports.retrieveProductusingID = (reqParams) => {

  try {
    const product = Product.findById(productId);

    console.log(product);
    
    if (!product) {
      return { error: "Product not found" };
    }
    return product;
  } catch (err) {
    console.log(err);
    return { error: "An error occurred while retrieving the product" };
  }
};

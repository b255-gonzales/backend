const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3000;


mongoose.connect("mongodb+srv://zenith-255:admin123@zuitt-bootcamp.acol4ye.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);


let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))


const userSchema = new mongoose.Schema({

	username: String,
	password: String
})

const User = mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res)=> {
	
	// Check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria as a single object.
	// findOne() can send the possible result or error in another method called then() for further processing.
	// .then() is chained to another method that is able to return a value or an error.
	// .then() waits for the previous method to complete its process. It will only run once the previous method is able to return a value or error.
	// .then() method can then process the returned result or error in a callback method inside it.
	// the callback method in the then() will be able to receive the result or error returned by the previous method it is attached to.
	// the .findOne() method returns the result first and the error second as parameters.
	// Call back functions in mongoose methods are programmed this way to store the returned results in the first parameter and any errors in the second parameter
	// If there are no matches, the value of result is null
	// "err" is a shorthand naming convention for errors
	User.findOne({username : req.body.username}).then((result, err) => {
		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.username == req.body.username ){
			// Return a message to the client/Postman
			return res.send("Duplicate user found");

		// If no document was found
		} else {

			// Create a new task and save it to the database
			

			let newUser = new User({
				username : req.body.username,
				password : req.body.password

			});


			newUser.save().then((savedUser, saveErr) => {

				if(saveErr){


					return console.error(saveErr);


				}else if(req.body.username == null || req.body.password == null){


						return res.status(404).send("BOTH username and password must be provided.");


				} else {


					return res.status(201).send("New user registered");

				}
			})
		}

	})
})

app.get("/tasks", (req, res) =>{

	User.find({}).then((result, err) =>{

		if(err) {
			return console.log(err)
		} else {
			return res.status(200).json({
				data:result
			})
		}
	})
})




















// Listen to the port, meaning, if the port is accessed we run the server
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));

}
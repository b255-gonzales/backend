console.log("HELLO WORLD");
// [SECTION] JSON Objects

/*
	-JSON stands for JavaScript Object Notation
	-JSON is also used in other programming languages hence the name JavaScript Object Notation
	-Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting data into a series of bytes for easier transmission/transfer of information
	-A byte is a unit of data that is eight binary digits(1 and 0) that is used to represent a character(letters, numbers or typographic symbols)
	-Bytes are information that a computer processes to perform different tasks

*/

// JSON Objects

/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON ARRAYS

/*"cities": [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
]
*/

// [SECTION] JSON methods

//  The JSON object contains methods for parsing and converting data into stringfield JSON

// [SECTION] Converting Data into stringfied JSON

/*
	-Stringified JSON is a JavaScript object converted into a string to be used in other functions of a JavaScript application
	-They are commonly used in HTTP requests where information is required to be sent and received in a stringfied JSON 
	- Requests are an important part of programming where an application communicates with the backend application to perform different tasks such as getting/creating data in a database
	-A fronted application is an application that is used to interact users to perform different visual tasks and display information while backend application are commonly used for all business logic and data processing
	-Commonly stored data in databases are user information, transaction records and product information
	-Node/Express JS are two types of technologies that are used for creating backend applications which processes requests from frontend applications

*/

let batchesArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'}];

console.log('Result rom stringify method:');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'

	}
});

console.log(data);

// [SECTION] using stringify method is Variables

/*
	- When information is stored in a varibale and is not hard coded into a an object that is being stringfied we can supply the value with a vaiable 
	- The "property" name and "value" would have the same name which can be confusing for beginners
	- This is done on puprose for code readability meaning when an information is stored in a variable and when the object created to be stringified is created, we suppliy the variable name insted of a hard coded value. 
	- This is commonly used when the information to be stored and sent to a backend application will come from a frontend application

*/

let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your address belong to?')
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);

// [SECTION] Converting stringfied JSON into JavaScript objects

/*
	-Objects are common data types used in application because of the complex data structure that can be created out of them
	-Information is commonly sent to application in stringfied JSON and then converted back into objects
	-This happens both for sending information to a backend application

*/

let batchesJSON = '[{"batchName": "Batch X"}, {"bactchName": "Batch Y"}]';

console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));











































fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(json => {
    const titles = json.map(todo => todo.title);
    console.log(titles);
  });

 // Number 5

fetch('https://jsonplaceholder.typicode.com/todos/1')

.then((response) => response.json())
.then((json) => console.log(json));

// Number 7

fetch('https://jsonplaceholder.typicode.com/todos', {

	method: 'POST',

	headers: {
		'Content-type': 'application/json', 
	},

	body: JSON.stringify({
		id: 201,
		title: 'Created To Do List Item',
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json));

// Number 8

fetch('https://jsonplaceholder.typicode.com/todos/1', {

	method: 'PUT',

	headers: {
		'Content-type': 'application/json', 
	},

	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with different data structure",
		id: 1,
		status: "Pending",
		title: 'Updated To Do List Item',
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json));


// Number 10
fetch('https://jsonplaceholder.typicode.com/todos/1', {

	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',

	}, 

	body: JSON.stringify({
		completed: false,
		dateCompleted: '07/09/21',
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json));



// DELETE

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'

});
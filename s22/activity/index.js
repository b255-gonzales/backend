console.log("HELLO UNIVERSE");

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function called register which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, return the message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and return the message:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

function register(newUser){

	if( registeredUsers.includes(newUser)){

		console.log("Registration failed. Username already exists!");

	}else {
		registeredUsers.push(newUser);

		console.log("Thank you for registering!");
	}
}
    


/*
    2. Create a function called addFriend which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then return the message with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, return the message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

function addFriend(availableUsers){

					if(friendsList.includes(availableUsers)){
						console.log(availableUsers + " is already your friend!");
					}

					else if( registeredUsers.includes(availableUsers) && friendsList.includes(availableUsers)!==true){

						let firstIndex = registeredUsers.indexOf(availableUsers);

						console.log("You have added " + registeredUsers[firstIndex] + " as a friend!");
						friendsList.push(availableUsers);

						console.log(friendsList);


					}else{

						console.log("User not found.");
					}



					

	
}



/*
    3. Create a function called displayFriends which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty return the message: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

function displayFriends(){

	if(friendsList.length > 0){

		friendsList.forEach(function(friends){

		console.log(friends);
		})

	}else {
		console.log("You currently have 0 friends. Add one first.");
	}
}
    


/*
    4. Create a function called displayNumberOfFriends which will display the amount of registered users in your friendsList.
        - If the friendsList is empty return the message:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty return the message:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function displayNumberOfFriends(){
	if (friendsList.length > 0) {
		
		console.log("You currently have " + friendsList.length + " friends.");
	}else {
		console.log("You currently have 0 friends. Add one first.");
	}

}


/*
    5. Create a function called deleteFriend which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty return a message:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

	function deleteFriend(){
			if (friendsList.length > 0) {
				friendsList.pop(friendsList.length);

			}else{

				console.log("You currently have 0 friends. Add one first.");
			}

	}

	function deleteFriendSpecificIndex(usersIndex){

		friendsList.splice(usersIndex, 1);

	}

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/








//For exporting to test.js
try {
    module.exports = {
        registeredUsers,
        friendsList,
        register,
        addFriend,
        displayFriends,
        displayNumberOfFriends,
        deleteFriend
    }
}
catch(err) {

}